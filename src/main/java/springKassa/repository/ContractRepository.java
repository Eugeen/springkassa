package springKassa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import springKassa.domain.Clients;
import springKassa.domain.Contract;
import springKassa.domain.Filial;

import java.util.List;

@Repository
public interface ContractRepository extends CrudRepository<Contract, Integer> {
    @Query("select c from Contract c order by c.rc")
    List<Contract> findAllContract();
}

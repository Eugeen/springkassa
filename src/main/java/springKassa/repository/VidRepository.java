package springKassa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import springKassa.domain.Vid;

import java.util.List;

@Repository
public interface VidRepository extends CrudRepository<Vid, Integer> {
    @Query("select v from Vid v")
    List<Vid> findAllVids();
}



package springKassa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import springKassa.domain.Clients;
import springKassa.domain.Filial;
import springKassa.domain.Kp;

import java.util.List;

@Repository
public interface FilialRepository  extends CrudRepository<Filial, Integer> {
    @Query("select f from Filial f order by f.address")
    List<Filial> findAllFilial();
}

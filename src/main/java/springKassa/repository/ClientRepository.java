package springKassa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import springKassa.domain.Clients;

import java.util.List;

@Repository
public interface ClientRepository extends CrudRepository<Clients, Integer> {
    @Query("select c from Clients c order by c.fio")
    List<Clients> findAllClients();

    @Query("select c from Clients c where c.id = ?1")
    Clients findClientByid(int id);

}

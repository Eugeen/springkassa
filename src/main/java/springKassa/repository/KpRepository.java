package springKassa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import springKassa.domain.Kp;

import java.util.List;

@Repository
public interface KpRepository extends CrudRepository<Kp, Integer> {
    @Query("select k from Kp k order by k.datetime")
    List<Kp> findAllKp();

    @Query("select count(k) from Kp k where k.clients.id = ?1")
    Integer operCountByClient(int clientid);
}

package springKassa.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import springKassa.domain.Clients;
import springKassa.domain.Kassir;

import java.util.List;

@Repository
public interface KassirRepository extends CrudRepository<Kassir, Integer> {
    @Query("select k from Kassir k order by k.fio")
    List<Kassir> findAllKassir();
}

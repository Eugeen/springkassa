package springKassa.dto;

import lombok.Data;

@Data
public class ClientDto {
    private int id;

    private String fio;

    private String country;

    private String city;

    private String street;

    private String selfid;

    private String docser;

    private String docnum;
}

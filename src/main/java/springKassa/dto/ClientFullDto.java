package springKassa.dto;

import lombok.Data;
import springKassa.domain.Clients;

@Data
public class ClientFullDto {

    private Clients clients;

    private int operCount;
}

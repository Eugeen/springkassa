package springKassa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import springKassa.domain.*;
import springKassa.domain.clientInfo.Address;
import springKassa.domain.clientInfo.Passport;
import springKassa.dto.ClientDto;
import springKassa.dto.ClientFullDto;
import springKassa.repository.*;

import java.util.List;

@Service
@Scope("session")
@Description("Сервис Просмотра Справочников")
public class KassaService {
    private final KpRepository kpRepository;

    private final ClientRepository clientRepository;

    private final VidRepository vidRepository;

    private final KassirRepository kassirRepository;

    private final ContractRepository contractRepository;

    @Autowired
    public KassaService(KpRepository kpRepository, ClientRepository clientRepository, VidRepository vidRepository, KassirRepository kassirRepository, ContractRepository contractRepository) {
        this.kpRepository = kpRepository;
        this.clientRepository = clientRepository;
        this.vidRepository = vidRepository;
        this.kassirRepository = kassirRepository;
        this.contractRepository = contractRepository;
    }

    public List<Kp> allKp() {
        return kpRepository.findAllKp();
    }

    public List<Clients> allClients() {
        return clientRepository.findAllClients();
    }

    public List<Vid> allVids() {
        System.out.println("allVids");
        return vidRepository.findAllVids();
    }

    public List<Kassir> allKassir() {
        return kassirRepository.findAllKassir();
    }

    public List<Contract> allContract() {
        return contractRepository.findAllContract();
    }

    private Clients clientById(int id) {
        Clients clients = clientRepository.findClientByid(id);
        return clients;
    }

    public ClientFullDto fullClientInfoById(int id){
        ClientFullDto clientFullDto = new ClientFullDto();
        Clients clients = clientRepository.findClientByid(id);
        clientFullDto.setClients(clients);
        clientFullDto.setOperCount(kpRepository.operCountByClient(clients.getId()));
        return clientFullDto;
    }

    public Clients saveClients(ClientDto clientDto) {
        Clients clients = new Clients();
        clients.setId(clientDto.getId());
        clients.setFio(clientDto.getFio());
        Address address = new Address();
        address.setCity(clientDto.getCity());
        address.setCountry(clientDto.getCountry());
        address.setStreet(clientDto.getStreet());
        Passport passport = new Passport();
        passport.setPassNum(clientDto.getDocnum());
        passport.setPassSer(clientDto.getDocser());
        passport.setSelfId(clientDto.getSelfid());
        clients.setPassport(passport);
        clients.setAddress(address);
        System.out.println(clientDto);
        clientRepository.save(clients);
        return clients;
    }
}

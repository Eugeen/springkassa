package springKassa.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "Filial")
public class Filial {
    @Id
    @GeneratedValue
    private int id;

    @Column(name = "address")
    private String address;

    @JsonIgnore
    @OneToMany(mappedBy = "filial",fetch = FetchType.LAZY)
    private List<Kassir> kassirs;
}

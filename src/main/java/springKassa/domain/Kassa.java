package springKassa.domain;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@Table(name = "Kassa")
public class Kassa {
    @Id
    @GeneratedValue
    private int id;

    @Column(name = "currency")
    private int currency;

    @Column(name = "money")
    private BigDecimal money;
}

package springKassa.domain.clientInfo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
@Data
public class Passport {
    @Column(name = "pass_num",nullable = false)
    private String passNum;

    @Column(name = "pass_ser",nullable = false)
    private String passSer;

    @Column(name = "self_id",nullable = false)
    private String selfId;
}

package springKassa.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "Contract")
public class Contract {
    @Id
    @GeneratedValue
    private int id;

    @Column(name = "RC")
    private String rc;

    @Column(name = "unp")
    private String unp;

    @Column(name = "date_begin")
    @Temporal(TemporalType.DATE)
    private Date dateBegin;

    @Column(name = "date_end")
    @Temporal(TemporalType.DATE)
    private Date dateEnd;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "Contracts_vids",
            joinColumns = @JoinColumn(name = "Contr_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "Vid_ID", referencedColumnName = "ID"))
    private List<Vid> vids;
}

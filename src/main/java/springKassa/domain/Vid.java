package springKassa.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Data
@Table(name = "Vid")
public class Vid {
    @Id
    @GeneratedValue
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "comiss_sum", nullable = false)
    private BigDecimal comissSum;

    @JsonIgnore
    @ManyToMany(mappedBy = "vids")
    private List<Contract> contracts;
}

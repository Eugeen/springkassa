package springKassa.domain;

import lombok.Data;
import springKassa.domain.clientInfo.Address;
import springKassa.domain.clientInfo.Passport;

import javax.persistence.*;

@Entity
@Data
@Table(name = "Clients")
@NamedQueries({
        @NamedQuery(name = "clientAll", query = "select cl from Clients cl order by cl.id"),
        @NamedQuery(name = "clientBySelfId", query = "select cl from Clients cl where cl.passport.selfId = :selfId")
})
public class Clients {
    @Id
    @GeneratedValue
    private int id;

    @Column(name = "fio", nullable = false)
    private String fio;

    @Embedded
    private Address address;

    @Embedded
    private Passport passport;
}

package springKassa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springKassa.domain.*;
import springKassa.dto.ClientDto;
import springKassa.dto.ClientFullDto;
import springKassa.service.KassaService;

import java.util.List;

@RestController
@RequestMapping("/kassa")
@ResponseBody
public class KassaController {
    //http://localhost:8081/kassa/contracts
    private final KassaService kassaService;

    @Autowired
    public KassaController(KassaService kassaService) {
        this.kassaService = kassaService;
    }

    @CrossOrigin //No 'Access-Control-Allow-Origin' header is present on the requested resource.
    @RequestMapping(value = "/kp", method = RequestMethod.GET)
    public List<Kp> allKp() {
        return kassaService.allKp();
    }

    @CrossOrigin
    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    public List<Clients> allClients() {
        return kassaService.allClients();
    }

    @CrossOrigin
    @RequestMapping(value = "/vids", method = RequestMethod.GET)
    public List<Vid> allVids() {
        return kassaService.allVids();
    }

    @CrossOrigin
    @RequestMapping(value = "/contracts", method = RequestMethod.GET)
    public List<Contract> allContracts() {
        return kassaService.allContract();
    }

    @CrossOrigin
    @RequestMapping(value = "/kassir", method = RequestMethod.GET)
    public List<Kassir> allKassir() {
        return kassaService.allKassir();
    }

    @CrossOrigin
    @PostMapping(value = "/saveclient")
    public Clients saveClient(@RequestBody ClientDto clientDto) {
        return kassaService.saveClients(clientDto);
    }

    @CrossOrigin
    @RequestMapping(value = "/clientById/{id}", method = RequestMethod.GET)
    public ClientFullDto saveClient(@PathVariable("id") Integer id) {
        return kassaService.fullClientInfoById(id);
    }

}
